﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace iDeviceLogParser
{
    internal class TailFile
    {
        private string filePath;
        private bool isRunning;
        public  Dictionary<String, String> testresult = new Dictionary<String, String>();

        public bool IsBegin {  get; set; }

        public TailFile(string filePath)
        {
            this.filePath = filePath;
            this.isRunning = true;
            IsBegin = false;
        }

        public void FunctionTest()
        {
            IsBegin = false;
            const String LogStart = "[ResultReport][";
            int nTotal = 0;
            try
            {//[MFT] [ResultReport] [1/5] :
                Regex reg = new Regex(@"\[ResultReport\]\[(\d+)\/(\d+)\]\[\d+\]:(.+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    if (IsBegin)
                    {
                        fs.Seek(0, SeekOrigin.Begin);
                    }
                    else
                    {
                        // Move to the end of the file
                        fs.Seek(0, SeekOrigin.End);
                    }
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        while (isRunning)
                        {
                            string line;
                            while ((line = sr.ReadLine()) != null)
                            {
                                if (line.Contains(LogStart))
                                {
                                    //Console.WriteLine(line);
                                    Match m = reg.Match(line);
                                    if (m.Success)
                                    {
                                        Program.logIt($"Index={m.Groups[1].Value}");
                                        Program.logIt($"Total={m.Groups[2].Value}");
                                        nTotal = nTotal == 0 ? Convert.ToInt32(m.Groups[2].Value) : nTotal;
                                        Program.logIt($"Detail={m.Groups[3].Value}");
                                        testresult[m.Groups[1].Value] = m.Groups[3].Value;
                                        // Zero is checksum
                                        if (testresult.Count == nTotal + 1)
                                        {
                                            Program.logIt($"all packages are received: {nTotal}");
                                            isRunning = false;
                                            return;
                                            //break;
                                        }
                                    }
                                }
                            }

                            // Sleep for a while before reading the file again
                            Thread.Sleep(10);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Program.logIt("An error occurred: " + ex.Message);
            }
            IsBegin = false;
        }
        public void BatteryMaxCapacity()
        {
            try
            {
                Regex reg = new Regex(@"Updated Battery Health: MaxCapacity:\s*(\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (StreamReader sr = new StreamReader(fs))
                {
                    if (IsBegin)
                    {
                        fs.Seek(0, SeekOrigin.Begin);
                    }
                    else
                    {
                        // Move to the end of the file
                        fs.Seek(0, SeekOrigin.End);
                    }

                    while (isRunning)
                    {
                        string line;
                        while ((line = sr.ReadLine()) != null)
                        {
                            //Console.WriteLine(line);
                            Match m = reg.Match(line);
                            if (m.Success)
                            {
                                Console.WriteLine($"MaxCapcity={m.Groups[1].Value}");
                                isRunning = false;
                                break;
                            }
                        }

                        // Sleep for a while before reading the file again
                        Thread.Sleep(10);
                    }
                }
            }
            catch (Exception ex)
            {
                Program.logIt("An error occurred: " + ex.Message);
            }
        }

        public void Stop()
        {
            isRunning = false;
        }
    }
}
