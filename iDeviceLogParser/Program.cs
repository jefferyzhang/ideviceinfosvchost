﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iDeviceLogParser
{
    internal class Program
    {
        static String TAG = "Label_0";
        static public void logIt(string s)
        {
            System.Diagnostics.Trace.WriteLine($"[iDeviceLogParser][{TAG}]: {s}");
            Console.WriteLine( s );
        }
        static int Main(string[] args)
        {
            System.Configuration.Install.InstallContext _arg = new System.Configuration.Install.InstallContext(null, args);
            if (_arg.IsParameterTrue("debug"))
            {
                System.Console.WriteLine("Wait for debugger, press any key to continue...");
                System.Console.ReadKey();
            }
            string _label = "";
            if (_arg.Parameters.ContainsKey("label"))
            {
                _label = _arg.Parameters["label"];
                TAG = $"Label_{_label}";
            }

            // dump version
            logIt(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileVersionInfo.ToString());
            // dump args
            logIt($"called by arg: ({args.Length}): {Environment.CommandLine}");
            foreach (string s in args)
                logIt(s);


            string filePath = @"E:\temp\APSTHOME\temp\00008110-00067C5C1A91401E.logcat";  // Replace with the actual file path
            if (_arg.Parameters.ContainsKey("file"))
            {
                filePath = _arg.Parameters["file"];
            }
            else
            {
                logIt("log file path not found. comand line error");
                return 1;
            }
            

            if (_arg.Parameters.ContainsKey("ppid"))
            {
                int ppid = Convert.ToInt32(_arg.Parameters["ppid"]);
                new Thread( () => { 
                    Process.GetProcessById(ppid).WaitForExit();
                    Process.GetCurrentProcess().Kill();
                }).Start();
            }

            //bool createnew;
            //var ewait = new EventWaitHandle(false, EventResetMode.ManualReset, $"logparser_label_{_label}", out createnew);
            //if (createnew) { logIt("Event is Created new."); }

            while (!File.Exists(filePath))
            {
                //if (ewait.WaitOne(0))
                //{
                //    return 10;
                //}
                Thread.Sleep(500);
            }
            int ret = 0;
            if (_arg.Parameters.ContainsKey("mft"))
            {
                TailFile tailFile = new TailFile(filePath);

                tailFile.IsBegin = true;
                //Thread tailThread = new Thread(tailFile.FunctionTest);
                //tailThread.Start();

                //tailThread.Join();
                tailFile.FunctionTest();
                logIt("tailFile.FunctionTest Exit");
                StringBuilder sb = new StringBuilder();
                if (tailFile.testresult.Count > 0)
                {
                    //Parser and output Json
                    for (int i = 1; i < tailFile.testresult.Count; i++)
                    {
                        if (!tailFile.testresult.ContainsKey(i.ToString()))
                        {
                            ret = 2;
                            break;
                        }
                        sb.Append(tailFile.testresult[i.ToString()]);
                    }
                    if (ret == 0)
                    {
                        //-pcfn="C:\ProgramData\Futuredial\TetherWing\info\result_label_3.func"
                        string decodedString = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(sb.ToString()));
                        File.WriteAllText(Environment.ExpandEnvironmentVariables($"%APSTHOME%..\\TetherWing\\info\\result_label_{_label}.func"), decodedString);
                    }
                }

            }
            logIt($"{Process.GetCurrentProcess().ProcessName} return {ret}");
            return ret;
        }
    }
}
